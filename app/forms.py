from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError

class SignUpForm(UserCreationForm):
    CHOICES=[('student','student'),
         ('teacher','teacher'),
         ('coordinator','coordinator')]
    username = forms.CharField(label='Username', min_length=3, max_length=150)
    first_name = forms.CharField(max_length=32, label='First name')
    last_name=forms.CharField( max_length=32, label='Last name')
    email=forms.EmailField(max_length=64, label='Email')
    password1=forms.CharField(widget=forms.PasswordInput,label='Password')
    password2=forms.CharField(widget=forms.PasswordInput, label='Password Verification')
    promotion=forms.CharField(max_length=64, label='Promotion', required=False)
    user_type = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect(), label='User type')

    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + ('first_name', 'last_name', 'email',)
