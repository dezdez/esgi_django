from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='app-home'),
    path('login/', views.login_views, name='app-login'),
    path('logout/', views.logout_views, name='app-logout'),
    path('student/',views.get_student_view),
    path('teacher/',views.get_teacher_view),
    path('coordinator/',views.get_coordinator_view),
    path('course/',views.get_course_view),
    path('create-user/',views.create_user, name='app-create_user'),

]