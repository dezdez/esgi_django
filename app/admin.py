from django.contrib import admin
from .models import Course, Promotion

# Register your models here.
admin.site.register(Course)
admin.site.register(Promotion)