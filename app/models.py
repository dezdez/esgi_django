from django.db import models
from datetime import datetime
from django.contrib.auth.models import User

# Create your models here.

#Model Course
class Course(models.Model):

    name = models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    teacher = models.ForeignKey(User,  null = True, related_name = "course", on_delete = models.PROTECT, verbose_name = "teacher")

    def str(self):
        return self.name, self.description


class Promotion(models.Model):
    year = models.IntegerField(default=datetime.now().year)
    students = models.ManyToManyField(User, related_name = "promotion", verbose_name = "students")
    courses = models.ManyToManyField(Course, related_name = "promotion", verbose_name = "courses")

    def str(self):
        return self.year