from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib import messages
from .forms import SignUpForm
from django.contrib.auth.models import Group, Permission
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from .models import Course
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.decorators import permission_required

def login_views(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            messages.success(request, f'Bienvenu {username} !')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            return redirect('app-home')
    else:
        form = AuthenticationForm()
    return render(request, 'app/login.html', {'form': form})

def logout_views(request):
    logout(request)
    messages.info(request, f'Vous avez été déconnecté!')
    return redirect('/login')

def create_user(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.save()
            username = form.cleaned_data.get('username')
            user_type = form.cleaned_data.get('user_type')
            print(user_type)
            if user_type == 'student':
                grp_student, created = Group.objects.get_or_create(name="Student")
                if created:
                    add_permissions_to_group_student(grp_student)
                user.groups.add(grp_student)
            elif user_type == 'teacher':
                grp_teacher, created = Group.objects.get_or_create(name="Teacher")
                if created:
                    add_permissions_to_group_teacher(grp_teacher)
                user.groups.add(grp_teacher)
            else:
                grp_coordinator, created = Group.objects.get_or_create(name="Coordinator")
                if created:
                    add_permissions_to_group_coordinator(grp_coordinator)
                user.groups.add(grp_coordinator)

            messages.success(request, f'User created {username} !')
            return HttpResponseRedirect(request.path_info)
    else:
        form = SignUpForm()
    return render(request, 'app/create-user.html', {'form': form})

def index(request):
    grp_student, created = Group.objects.get_or_create(name="Student")
    print(grp_student, created)
    return render(request, 'app/home.html')

def get_student_view(request):
    return render(request, 'student/student.html')

def get_teacher_view(request):
    return render(request, 'teacher/teacher.html')

def get_coordinator_view(request):
    return render(request, 'coordinator/coordinator.html')

def get_course_view(request):
    courses = Course.objects.all()
    return render(request, 'app/course/course.html', {'courses': courses})



def add_permissions_to_group_student(grp_student):
    permission_can_read_course = Permission.objects.get(name="Can view course")
    permission_can_read_promotion = Permission.objects.get(name="Can view promotion")

    grp_student.permissions.add(permission_can_read_course, permission_can_read_promotion)

def add_permissions_to_group_teacher(grp_teacher):
    permission_can_read_course = Permission.objects.get(name="Can view course")
    permission_can_read_promotion = Permission.objects.get(name="Can view promotion")

    grp_teacher.permissions.add(permission_can_read_course, permission_can_read_promotion)

def add_permissions_to_group_coordinator(grp_coordinator):
    permission_can_read_course = Permission.objects.get(name="Can view course")
    permission_can_add_course = Permission.objects.get(name="Can add course")
    permission_can_change_course = Permission.objects.get(name="Can change course")
    permission_can_delete_course = Permission.objects.get(name="Can delete course")

    permission_can_read_promotion = Permission.objects.get(name="Can view promotion")
    permission_can_add_promotion = Permission.objects.get(name="Can add promotion")
    permission_can_change_promotion = Permission.objects.get(name="Can change promotion")
    permission_can_delete_promotion = Permission.objects.get(name="Can delete promotion")

    permission_can_read_user = Permission.objects.get(name="Can view user")
    permission_can_add_user = Permission.objects.get(name="Can add user")
    permission_can_change_user = Permission.objects.get(name="Can change user")
    permission_can_delete_user = Permission.objects.get(name="Can delete user")
    
    grp_coordinator.permissions.add(permission_can_read_course)
    grp_coordinator.permissions.add(permission_can_add_course)
    grp_coordinator.permissions.add(permission_can_change_course)
    grp_coordinator.permissions.add(permission_can_delete_course)
    grp_coordinator.permissions.add(permission_can_read_promotion)
    grp_coordinator.permissions.add(permission_can_add_promotion)
    grp_coordinator.permissions.add(permission_can_change_promotion)
    grp_coordinator.permissions.add(permission_can_delete_promotion)
    grp_coordinator.permissions.add(permission_can_read_user)
    grp_coordinator.permissions.add(permission_can_add_user)
    grp_coordinator.permissions.add(permission_can_change_user)
    grp_coordinator.permissions.add(permission_can_delete_user)